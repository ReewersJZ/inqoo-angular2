import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlbumSearchService } from '../core/services/album-search/album-search.service';
import { AlbumSearchComponent } from './containers/album-search/album-search.component';
import { MusicComponent } from './music.component';

const routes: Routes = [{
  // app => music / ***
  path: '',
  component: MusicComponent,
  children: [
    {
      path: 'search',
      component: AlbumSearchComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicRoutingModule { }
