import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Album } from 'src/app/core/model/album';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {

  // @Input('albums') albums: Album[] | undefined;
  @Output('searched') searched = new EventEmitter();

  searchElem: string = '';
  // draftSearch: string = '';
  // result: Album = {
  //   id: '',
  //   name: '',
  //   images: []
  // };


  search(elem: any){
    console.log('search elem', elem);
    this.searched.emit(elem);  }

  constructor() { }

  ngOnInit(): void {
    
    // console.log("search elem", this.searchElem);
    // console.log("search-form", this.albums)
  }

}
