import { Component, OnInit } from '@angular/core';
import { Playlist } from '../../model/Playlist';

@Component({
  selector: 'app-playlists-view',
  templateUrl: './playlists-view.component.html',
  styleUrls: ['./playlists-view.component.scss'],
})
export class PlaylistsViewComponent implements OnInit {
  mode: 'details' | 'edit' = 'details';

  playlists: Playlist[] = [
    {
      id: '123',
      name: 'Playlist 123',
      public: false,
      description: 'my favourite playlist',
    },
    {
      id: '456',
      name: 'Playlist 456',
      public: true,
      description: 'my favourite b',
    },
    {
      id: '789',
      name: 'Playlist 789',
      public: false,
      description: 'my favourite c',
    },
  ];

  index: number | undefined;

  // selected = this.playlists[1];
  selected: Playlist | undefined;

  constructor() {}

  edit($event: any) {
    this.mode = 'edit';
    console.log('edit in parent ', $event);
    this.selected = $event;
  }

  cancel($event: Playlist) {
    this.mode = 'details';
    console.log('canceled window edit ', $event);
    // this.playlists = this.playlists.filter((i) => i.id !== $event.id);
  }

  deleteEl($event: Playlist){
    this.mode = 'details';
    console.log('delete in parent ', $event);
    this.playlists = this.playlists.filter((i) => i.id !== $event.id);
  }

  add(){
    this.mode = "edit";
    this.selected = {id:"", name:"", description:"", public: false};
  }

  save($event: Playlist){
    this.index  = this.playlists.findIndex(i => i.id === $event.id);
    if(this.index === -1){
      this.playlists.push($event);
      
    }
    else{
      this.playlists[this.index] = $event;
      console.log(this.playlists);
    }

    this.selected = undefined;
    this.mode = 'details';

  }

  ngOnInit(): void {}
}
