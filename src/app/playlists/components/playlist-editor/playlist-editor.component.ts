import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from '../../model/Playlist';

@Component({
  selector: 'app-playlist-editor',
  templateUrl: './playlist-editor.component.html',
  styleUrls: ['./playlist-editor.component.scss']
})
export class PlaylistEditorComponent implements OnInit {

  playlist = {
    id:'123',
    name:'Playlist 123',
    public:false,
    description:'my favourite playlist'
  };

  @Input('selectedDetails') selected:any = {};
  @Output('editedCancel') selectedPlaylist = new EventEmitter;
  @Output('editedDelete') deletePlaylist = new EventEmitter;
  @Output('saveEl') saveEl = new EventEmitter;

  draft: any = {};

  constructor() { }

  ngOnChanges(): void {
    this.draft = {...this.selected};
  }

  click(selected: Playlist){
    // console.log('click  function', selected);
    this.selectedPlaylist.emit(selected);
  }

  deleteEl(selected: Playlist){
    console.log('cdeleteEl  function', selected);
    this.deletePlaylist.emit(selected);
  }

  save(draft: Playlist){
    this.saveEl.emit(draft)
  }

  ngOnInit(): void {
  }

}
