import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from '../../model/Playlist';

@Component({
  selector: 'app-playlists-list',
  templateUrl: './playlists-list.component.html',
  styleUrls: ['./playlists-list.component.scss'],

})
export class PlaylistsListComponent implements OnInit {

@Input('items') playlists: Playlist []= [];
@Input() selected?: Playlist;
@Input() mode: string | undefined;

@Output() selectedChange = new EventEmitter<Playlist>();
@Output() addEl = new EventEmitter();

// selected: any = {};



select(playlist: Playlist){
  this.selectedChange.emit(playlist)
  // if(this.selected.id === item.id){
  //   this.selected = {};
  //   console.log("nothing selected");
  // }
  // else{
    // this.selected = {...item};
    // console.log(this.selected);
  // }
  
}

  constructor() { }

  add(){
    console.log("add new test");
    this.selected = undefined;
    this.addEl.emit(this.selected);
    // this.selected.emit({});


  }

  ngOnInit(): void {
  }

}


