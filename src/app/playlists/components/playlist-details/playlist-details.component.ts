import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from '../../model/Playlist';

@Component({
  selector: 'app-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.scss']
})
export class PlaylistDetailsComponent implements OnInit {

 
  @Input('selectedDetails') selected!: Playlist;
  @Output('editedDetails') selectedPlaylist = new EventEmitter;
  

  // @Output() selectedChange = new EventEmitter<Playlist>();


  fontSize = 20;

  constructor() { }

  click(selected: Playlist){
    console.log('click  function', selected);
    this.selectedPlaylist.emit(selected);
  }

  ngOnInit(): void {

    
  }

}
